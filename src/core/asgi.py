"""
ASGI config for help_me project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
from django.core.asgi import get_asgi_application

django_asgi_app = get_asgi_application()

from channels.auth import AuthMiddlewareStack
from rest_framework.authtoken.models import Token

from channels.db import database_sync_to_async
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import OriginValidator
from django.contrib.auth.models import AnonymousUser

import api.urls
from api.models import User


@database_sync_to_async
def get_user(token):
    try:
        return Token.objects.get(key=token).user
    except User.DoesNotExist:
        return AnonymousUser()


class QueryAuthMiddleware:
    """
    Custom middleware (insecure) that takes user IDs from the query string.
    """

    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        try:
            token_key = (dict((x.split('=') for x in scope['query_string'].decode().split("&")))).get('token', None)
        except ValueError:
            token_key = None
        scope['user'] = AnonymousUser() if token_key is None else await get_user(token_key)
        return await self.app(scope, receive, send)
        # token = None
        #
        # for pair in scope["headers"]:
        #     if pair[0] == b'authorization':
        #         token = pair[1].decode("utf-8").split(" ")[1]
        #
        # await get_user(token)
        #
        # return await self.app(scope, receive, send)


application = ProtocolTypeRouter({
    "http": django_asgi_app,
    "websocket": OriginValidator(
        QueryAuthMiddleware(
            URLRouter(
                api.urls.websocket_urlpatterns
            )
        ),
        ["*"],
    ),
})
