# Generated by Django 4.0.3 on 2022-05-06 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_applicationforhelp_offeredhelp_rating_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userimage',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='userimage',
            name='image_path',
        ),
        migrations.RemoveField(
            model_name='userimage',
            name='updated_at',
        ),
        migrations.AddField(
            model_name='userimage',
            name='path',
            field=models.ImageField(null=True, upload_to='media'),
        ),
        migrations.AlterField(
            model_name='applicationforhelpimage',
            name='path',
            field=models.ImageField(null=True, upload_to='media'),
        ),
        migrations.AlterField(
            model_name='offeredhelpimage',
            name='path',
            field=models.ImageField(null=True, upload_to='media'),
        ),
    ]
