# Generated by Django 4.0.3 on 2022-05-16 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_user_groups_user_is_superuser_user_user_permissions_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicationforhelp',
            name='place',
            field=models.TextField(null=True),
        ),
    ]
