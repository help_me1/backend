# Generated by Django 4.0.3 on 2022-05-06 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_rename_path_applicationforhelpimage_img_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicationforhelpimage',
            name='img',
            field=models.ImageField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='offeredhelpimage',
            name='img',
            field=models.ImageField(null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='userimage',
            name='img',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
