# Generated by Django 4.0.3 on 2022-05-17 21:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0011_chat_message_applicationforhelpimage_uploaded_at_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='applicationforhelpimage',
            options={'ordering': ('-uploaded_at',)},
        ),
        migrations.AlterModelOptions(
            name='messageimages',
            options={'ordering': ('-uploaded_at',)},
        ),
        migrations.AlterModelOptions(
            name='offeredhelpimage',
            options={'ordering': ('-uploaded_at',)},
        ),
        migrations.AlterModelOptions(
            name='userimage',
            options={'ordering': ('-uploaded_at',)},
        ),
    ]
