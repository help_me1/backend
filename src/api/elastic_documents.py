from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl.registries import registry

from api.models import ApplicationForHelp


# @registry.register_document
# class ApplicationForHelpDocument(Document):
#     class Index:
#         name = 'applications'
#         settings = {
#             'number_of_shards': 1,
#             'number_of_replicas': 0,
#         }
#
#     class Django:
#         model = ApplicationForHelp
#         fields = [
#             'id',
#             'description',
#             'title'
#         ]