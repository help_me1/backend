from django.db import models

from api.utils.image import change_filename


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class BaseImageModel(models.Model):
    img = models.ImageField(upload_to=change_filename, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True
        ordering = ('-uploaded_at',)