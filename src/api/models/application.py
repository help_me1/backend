from django.db import models

from api.models import User
from api.models.base import BaseModel, BaseImageModel


class TagsForApplication(BaseModel):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class ApplicationForHelp(BaseModel):
    user = models.ForeignKey(User, related_name="applications", on_delete=models.CASCADE)
    tags = models.ManyToManyField(TagsForApplication, related_name="applications")
    title = models.CharField(max_length=50)
    description = models.TextField()
    is_anonymous = models.BooleanField(default=False)
    place = models.TextField(null=True)
    elastic_doc_index = models.TextField(null=True)

    class Meta:
        ordering = ('-updated_at', "-created_at")


class ApplicationForHelpImage(BaseImageModel):
    help_application = models.ForeignKey(ApplicationForHelp, related_name="images", on_delete=models.CASCADE)
