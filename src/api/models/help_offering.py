from django.db import models

from api.models import User
from api.models.application import ApplicationForHelp
from api.models.base import BaseModel, BaseImageModel


class OfferedHelp(BaseModel):
    user = models.ForeignKey(User, related_name="help_offerings", on_delete=models.CASCADE)
    message = models.TextField()
    help_application = models.ForeignKey(ApplicationForHelp, on_delete=models.CASCADE, related_name="offered_help")
    is_taken = models.BooleanField(default=None,null=True)


class OfferedHelpImage(BaseImageModel):
    offered_help = models.ForeignKey(OfferedHelp, related_name="images", on_delete=models.CASCADE)
