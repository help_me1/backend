from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models

from api.models.base import BaseModel, BaseImageModel


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save()
        return user

    def create_staffuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    objects = UserManager()

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    is_active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)  # a admin user; non super-user
    admin = models.BooleanField(default=False)  # a superuser

    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)

    description = models.TextField(null=True)

    rating = models.ManyToManyField('self', through='Rating')

    USERNAME_FIELD = 'email'

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin


class UserImage(BaseImageModel):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='images', null=True)


class Rating(models.Model):
    reacter = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reacted_to")
    reacted_to = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reacted_by")
    is_positive = models.BooleanField(default=True)
