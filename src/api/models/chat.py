from django.db import models

from api.models import User, BaseImageModel, ApplicationForHelp


class Chat(models.Model):
    taker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='taker')
    helper = models.ForeignKey(User, on_delete=models.CASCADE, related_name='helper')
    application = models.ForeignKey(ApplicationForHelp, on_delete=models.CASCADE, related_name="chats")
    room_code = models.TextField(null=False, unique=True)


class Message(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='receiver')
    message = models.CharField(max_length=1200)
    sent_at = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name="messages")

    def __str__(self):
        return self.message

    class Meta:
        ordering = ('sent_at',)


class MessageImages(BaseImageModel):
    message = models.ForeignKey(Message, on_delete=models.CASCADE, related_name="images")
