from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from api.models import ApplicationForHelp, ApplicationForHelpImage, TagsForApplication
from api.serializers import BaseImageSerializer, BasePutImageSerializer, UserSerializer


class ApplicationImageSerializer(BaseImageSerializer):
    class Meta:
        fields = ('id', 'img')
        read_only_fields = ('id',)
        model = ApplicationForHelpImage


class ApplicationTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagsForApplication
        fields = ('name', 'id')


class PutApplicationImageSerializer(BasePutImageSerializer):
    def create(self, validated_data, **kwargs):
        help_application_id = kwargs['help_application_id']
        appl_img = ApplicationForHelpImage.objects.create(help_application_id=help_application_id,
                                                          img=validated_data['img'])
        self.instance = appl_img


class ApplicationSerializer(serializers.ModelSerializer):
    images = ApplicationImageSerializer(many=True, read_only=True)
    tags = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=TagsForApplication.objects.all(),
        required=False
    )
    user = UserSerializer(many=False, read_only=True, allow_null=True)
    replies_count = serializers.SerializerMethodField(read_only=True)

    @swagger_serializer_method(serializer_or_field=serializers.IntegerField)
    def get_replies_count(self, obj):
        return obj.offered_help.count()

    def create(self, validated_data, **kwargs):
        tags = validated_data.pop('tags')
        application = ApplicationForHelp.objects.create(**validated_data, user_id=kwargs['user_id'])
        application.tags.add(*tags)
        self.fake_data = dict(validated_data)
        tags_name = []
        for i in tags:
            tags_name.append(i.name)
        self.fake_data['tags'] = tags_name
        self.fake_data['id'] = application.id
        return application

    class Meta:
        model = ApplicationForHelp
        fields = (
            'id', 'tags', 'title', 'description', 'is_anonymous', 'images', 'place', 'created_at', 'user',
            'replies_count')
        read_only_fields = ('id',)
