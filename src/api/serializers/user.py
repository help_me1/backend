from rest_framework import serializers

from api.models import User
from api.models.user import UserImage
from api.serializers.base import BaseImageSerializer, BasePutImageSerializer


class UserImageSerializer(BaseImageSerializer):
    class Meta:
        fields = ('id', 'img')
        read_only_fields = ('id',)
        model = UserImage


class PutUserImageSerializer(BasePutImageSerializer):
    def create(self, validated_data, **kwargs):
        user_id = kwargs['user_id']
        usr_img = UserImage.objects.create(user_id=user_id, img=validated_data['img'])
        self.instance = usr_img


class UserSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True, read_only=True)

    def get_likes(self, obj):
        return

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'surname', 'description', 'images')
        read_only_fields = ['id', 'is_active', 'staff', 'admin']


class UserCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = self.Meta.model.objects.create_user(**validated_data)
        user.password = None
        return user

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'surname', 'description', 'password')
        read_only_fields = ['id', ]
