from rest_framework import serializers


class BasePutImageSerializer(serializers.Serializer):
    img = serializers.FileField()


class BaseImageSerializer(serializers.ModelSerializer):
    img = serializers.CharField()

    def create(self, validated_data, **kwargs):
        return self.Meta.model.objects.create(**validated_data, **kwargs)
