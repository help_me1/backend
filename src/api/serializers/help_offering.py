from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from api.models import OfferedHelp, User, OfferedHelpImage
from api.models.chat import Chat
from api.serializers import BaseImageSerializer
from api.utils.generating import create_random_chars


class UserForOfferingSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email", "name", "surname")


class OfferHelpImageSerializer(BaseImageSerializer):
    def create(self, validated_data, **kwargs):
        offered_help_id = kwargs['offered_help_id']
        offrd_img = OfferedHelpImage.objects.create(offered_help_id=offered_help_id,
                                                    img=validated_data['img'])
        self.instance = offrd_img

    class Meta:
        fields = ('id', 'img')
        read_only_fields = ('id',)
        model = OfferedHelpImage


class OfferedHelpSerializer(serializers.ModelSerializer):
    user = UserForOfferingSerializer(read_only=True, many=False)
    images = OfferHelpImageSerializer(many=True, read_only=True)

    def create(self, validated_data, **kwargs):
        application_id = kwargs.get('help_application_pk')
        return self.Meta.model.objects.create(**validated_data, help_application_id=application_id,
                                              user=kwargs.get('user'))

    class Meta:
        model = OfferedHelp
        fields = ('id', 'message', 'user', 'images', 'is_taken')
        read_only_fields = ('is_taken',)


class AcceptHelpOfferingSerializer(serializers.Serializer):
    accept = serializers.BooleanField()

    def update(self, instance: OfferedHelp, validated_data, **kwargs):
        instance.is_taken = validated_data['accept']

        instance.save()
        if validated_data['accept']:
            Chat.objects.create(room_code=create_random_chars(),
                                application=instance.help_application, taker_id=instance.help_application.user_id,
                                helper_id=instance.user_id)

        return instance
