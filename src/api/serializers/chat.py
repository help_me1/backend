from rest_framework import serializers

from api.models import User, ApplicationForHelp
from api.models.chat import Message, Chat
from api.serializers import UserImageSerializer


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.SlugRelatedField(many=False, slug_field='email', queryset=User.objects.all())
    receiver = serializers.SlugRelatedField(many=False, slug_field='email', queryset=User.objects.all())

    class Meta:
        model = Message
        fields = ['sender', 'receiver', 'message', 'sent_at', 'is_read']
        read_only_fields = ['sent_at', 'is_read']


class UserSerializerChatListSerializer(serializers.ModelSerializer):
    images = UserImageSerializer(many=True, read_only=True, allow_null=True)

    class Meta:
        model = User
        fields = ("id", "email", "name", "surname", 'images')


class ApplicationForHelpChatListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationForHelp
        fields = ("id", "title")


class ChatListSerializer(serializers.ModelSerializer):
    # unread = serializers.IntegerField()
    helper = UserSerializerChatListSerializer(read_only=True, many=False)
    taker = UserSerializerChatListSerializer(read_only=True, many=False)
    application = ApplicationForHelpChatListSerializer(read_only=True, many=False)
    latest_message = serializers.CharField(read_only=True,allow_null=True)

    class Meta:
        model = Chat
        fields = ("id", "room_code", "application", "helper", "taker", 'latest_message')
        read_only_fields = ("id", "room_code")


class ChatMessagesSerializer(serializers.ModelSerializer):
    sender = UserSerializerChatListSerializer(read_only=True, many=False)
    receiver = UserSerializerChatListSerializer(read_only=True, many=False)

    class Meta:
        model = Message
        fields = ("id", "sent_at", "sender", "receiver", 'message', 'is_read')
