from django.db.models import Q, Count, Prefetch, OuterRef
from rest_framework import viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.models import ApplicationForHelp, OfferedHelp, UserImage
from api.models.chat import Chat, Message
from api.serializers.chat import ChatListSerializer, ChatMessagesSerializer


class ChatListPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.auth:
            return False
        return Token.objects.exists(key=request.auth)


class ChatViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    serializer_class = ChatListSerializer
    permission_classes = [ChatListPermission]

    def get_object(self):
        last_message = Message.objects.filter(
            chat=OuterRef('pk')
        ).order_by('-sent_at').values('message')
        return Chat.objects.annotate(latest_message=last_message[:1]).get(id=self.kwargs.get('pk'))

    def get_queryset(self):
        user = Token.objects.get(key=self.request.auth).user
        last_message = Message.objects.filter(
            chat=OuterRef('pk')
        ).order_by('-sent_at').values('message')
        query = Chat.objects.filter(Q(taker=user) | Q(helper=user))
        return query \
            .annotate(latest_message=last_message[:1]) \
            .select_related('helper', 'taker', 'application') \
            .prefetch_related(
            'helper__images'
        )


class MessagesPermissions(BasePermission):
    def has_permission(self, request, view):
        chat_id = view.kwargs['chat_id']
        if not request.auth:
            return False
        user = Token.objects.get(key=request.auth).user
        return Chat.objects.filter(Q(Q(taker=user) | Q(helper=user)) & Q(id=chat_id)).exists()


class MessagesViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.UpdateModelMixin):
    # serializer_class = ChatMessagesSerializer
    permission_classes = [MessagesPermissions]

    def get_serializer_class(self):
        if self.action == 'list':
            return ChatMessagesSerializer
        if self.action == 'update':
            return None

    def get_queryset(self):
        return Message.objects.filter(chat_id=self.kwargs.get('chat_id')).select_related('receiver',
                                                                                         'sender').prefetch_related(
            'receiver__images', 'sender__images')

    def update(self, request, *args, **kwargs):
        token = Token.objects.get(key=request.auth)
        Message.objects.filter(chat_id=kwargs.get('chat_id'), receiver_id=token.user_id,
                               id__lte=kwargs.get('id')).update(is_read=True)
        return Response(status=200)
