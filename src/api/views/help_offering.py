from rest_framework import viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.models import OfferedHelp, ApplicationForHelp, OfferedHelpImage
from api.serializers.help_offering import OfferedHelpSerializer, OfferHelpImageSerializer, AcceptHelpOfferingSerializer


class OfferHelpPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.auth:
            return False

        user = Token.objects.get(key=request.auth).user

        if request.method == "GET":
            return obj.help_application.user_id == user.id

        if request.method == "POST":
            return obj.help_application.user_id != user.id

        if request.method == "PUT" or request.method == "DELETE":
            return obj.user_id == user.id


class HelpOfferingModelViewSet(viewsets.ModelViewSet):
    serializer_class = OfferedHelpSerializer
    permission_classes = [OfferHelpPermission]

    def get_queryset(self):
        return OfferedHelp.objects.filter(help_application_id=self.kwargs['help_application_pk']).select_related(
            "help_application", "user").prefetch_related("images")

    def get_object(self):
        return OfferedHelp.objects.get(id=self.kwargs['id']).select_related(
            "help_application", "user").prefetch_related("images")

    def create(self, request, *args, **kwargs):
        user = Token.objects.get(key=request.auth).user
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, help_application_pk=kwargs.get('help_application_pk'),
                          user=user)
        return Response(status=201, data=serializer.data)


class PictureOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.offered_help.user_id == request.user.id


class HelpOfferingImagesModelViewSet(viewsets.GenericViewSet, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = OfferHelpImageSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [PictureOnlyForOwnerPermission]
    queryset = OfferedHelpImage.objects.all()

    def update(self, request, *args, **kwargs):
        # print(request.data['image'])
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, offered_help_id=self.kwargs['offered_help_id'])
        return Response(status=204, data=serializer.data)


class AcceptHelpOfferingOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        user = Token.objects.get(key=request.auth).user
        help_application_id = view.kwargs.get('help_application_id')
        help_application = ApplicationForHelp.objects.get(id=help_application_id)
        return user.id == help_application.user_id


class AcceptHelpOffering(viewsets.GenericViewSet, mixins.UpdateModelMixin):
    serializer_class = AcceptHelpOfferingSerializer
    permission_classes = [AcceptHelpOfferingOnlyForOwnerPermission]

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.update(instance=OfferedHelp.objects.get(id=self.kwargs['offered_help_pk']),
                          validated_data=serializer.validated_data)
        return Response(status=200)
