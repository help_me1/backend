import random

import coreapi
import coreschema
from django.db.models import Q
from rest_framework import viewsets, mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import BasePermission
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser, FormParser, MultiPartParser
# from elasticsearch_dsl import Q as ELASTIC_Q
from rest_framework.schemas import ManualSchema

# from api.elastic_documents import ApplicationForHelpDocument
from api.models import ApplicationForHelp, TagsForApplication
from api.serializers import ApplicationSerializer, ApplicationImageSerializer, PutApplicationImageSerializer, \
    ApplicationTagSerializer


class PictureOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.help_application.user_id == request.user.id


class ApplicationEndpointsPermissions(BasePermission):
    ANY_USER_METHODS = ["GET"]

    def has_object_permission(self, request, view, obj):
        if request.method in self.ANY_USER_METHODS:
            return True

        if not request.auth:
            return False
        user = Token.objects.get(key=request.auth).user
        request.user = user
        if request.method == "POST" and user:
            return True

        return obj.user_id == user.id


class ApplicationModelViewSet(viewsets.ModelViewSet):
    schema = ManualSchema(fields=[
        coreapi.Field(
            "mine",
            required=False,
            location="query",
            schema=coreschema.Boolean(),
        ),
        coreapi.Field(
            "q",
            required=False,
            location="query",
            schema=coreschema.String(),
            description='Поиск в названии или описании'
        ),
        coreapi.Field(
            "tags",
            required=False,
            location='query',
            schema=coreschema.Array(),
            description='Имена тегов через запятую'
        )
    ])

    pagination_class = PageNumberPagination
    permission_classes = [ApplicationEndpointsPermissions]
    serializer_class = ApplicationSerializer

    def get_object(self):
        return ApplicationForHelp.objects.get(id=self.kwargs.get('pk'))

    def get_queryset(self):
        mine = self.request.query_params.get('mine')
        q = self.request.query_params.get('q')
        tags = self.request.query_params.get('tags')
        filtering = Q()

        if tags:
            filtering &= Q(tags__name__in=tags.split(','))

        if q:
            # elastic_query = ELASTIC_Q(
            #     'multi_match',
            #     query=q,
            #     fields=['title', 'description'],
            #     fuzziness='3'
            # )
            #
            # search = ApplicationForHelpDocument.search().query(elastic_query)
            # response = search.execute()
            # ids = []
            # for hit in search:
            #     ids.append(hit.id)
            # filtering &= Q(id__in=ids)
            filtering &= Q(title__contains=q) | Q(description__contains=q)

        if mine == 'true':
            filtering &= Q(user=self.request.user)
        elif self.request.user.is_authenticated:
            filtering &= ~Q(user=self.request.user)
        # apps = (
        #     ApplicationForHelp.objects
        #         .annotate(
        #         not_anonymous=FilteredRelation(
        #             'user',
        #             condition=Q(is_anonymous=False)
        #         )
        #     )
        #         .filter(not_anonymous__username__in=usernames)
        #         .select_related('user')
        # )
        return ApplicationForHelp.objects.filter(filtering).select_related(
            'user').prefetch_related('tags', 'user__images', 'images')

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, user_id=request.user.id)

        return Response(status=201, data=serializer.fake_data)


class ApplicationImageViewSet(viewsets.GenericViewSet, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = PutApplicationImageSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [PictureOnlyForOwnerPermission]

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, help_application_id=self.kwargs['help_application_id'])
        return Response(status=204, data=serializer.data)


class TagsListView(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ApplicationTagSerializer
    queryset = TagsForApplication.objects.all()
