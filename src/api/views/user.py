import random

from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.pagination import PageNumberPagination
from rest_framework import mixins
from rest_framework.parsers import FileUploadParser, FormParser, MultiPartParser
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.models import User, UserImage
from api.serializers import UserSerializer
from api.serializers.user import UserImageSerializer, UserCreateSerializer, PutUserImageSerializer


class UserEndpointsPermissions(BasePermission):
    ANY_USER_METHODS = ["GET", "POST"]

    def has_object_permission(self, request, view, obj):
        if request.method in self.ANY_USER_METHODS:
            return True

        if not request.auth:
            return False
        user = Token.objects.get(key=request.auth).user
        return obj.id == user.id


class UserApiViewModelSet(viewsets.ModelViewSet):
    """Пользователи"""
    queryset = User.objects.all().prefetch_related('images')
    pagination_class = PageNumberPagination
    permission_classes = [UserEndpointsPermissions]

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreateSerializer
        return UserSerializer

    @action(detail=False, methods=['GET'], name='Get Current User', url_path="info")
    def current_user(self, request, *args, **kwargs):
        if not request.auth:
            raise PermissionDenied(detail="Ауттенфицируйтесь.")

        token = Token.objects.get(key=request.auth)

        user = User.objects.get(id=token.user_id)

        serializer = self.get_serializer(user, many=False)
        return Response(serializer.data)


class PictureOnlyForOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        return obj.user_id == request.user.id


class UserImageViewSet(viewsets.GenericViewSet, mixins.UpdateModelMixin, mixins.DestroyModelMixin):
    serializer_class = PutUserImageSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [PictureOnlyForOwnerPermission]

    def get_queryset(self):
        return UserImage.objects.filter(user_id=self.kwargs['id'])

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, user_id=self.kwargs['user_id'])
        return Response(status=204, data=serializer.data)


# class LikeUserPermission(BasePermission):
#     def has_object_permission(self, request, view, obj):
#         user_id = view.kwargs.get('user_id')
#         user = Token.objects.get(key=request.auth).user
#         request.user = user
#         return user_id != user
#
#
# class LikeUserView(viewsets.GenericViewSet, mixins.CreateModelMixin):
#     serializer_class = LikeUserSerializer
#     permission_classes = [LikeUserPermission]
#
#     def create(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.create(serializer.validated_data, user_id=self.kwargs['user_id'], liker=request.user)
#
#         serialized_user = UserSerializer(instance=User.objects.get(id=self.kwargs['user_id']))
#         return Response(status=204, data=serialized_user.data)
