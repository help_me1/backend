from rest_framework.decorators import api_view
from rest_framework.response import Response
from .user import UserApiViewModelSet,UserImageViewSet
from .application import ApplicationModelViewSet
from .help_offering import HelpOfferingModelViewSet

@api_view(["GET"])
def main_api_view(request):
    return Response({"status": "ok"})
