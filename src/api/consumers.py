import json

from asgiref.sync import async_to_sync
from channels.db import database_sync_to_async
from channels.generic.websocket import WebsocketConsumer

from api.models.chat import Message, Chat


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_code = self.scope['url_route']['kwargs']['room_code']
        self.room_group_code = 'chat_%s' % self.room_code

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_code,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_code,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        sender_id = text_data_json['sender_id']
        receiver_id = text_data_json['receiver_id']
        # Send message to room group

        msg_db = self.save_message_db(sender_id=sender_id, receiver_id=receiver_id,
                                      message=message)
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_code,
            {
                'type': 'chat_message',
                'message': message,
                'sender_id': sender_id,
                'receiver_id': receiver_id
            }
        )

    def save_message_db(self, message: str, receiver_id: int, sender_id: int):
        chat = Chat.objects.get(room_code=self.room_code)
        return Message.objects.create(message=message, receiver_id=receiver_id, sender_id=sender_id, chat=chat)

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        sender_id = event['sender_id']
        receiver_id = event['receiver_id']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message,
            'sender_id': sender_id,
            'receiver_id': receiver_id,
        }))
