from django.urls import path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.authtoken import views as auth_views
from rest_framework_nested import routers

from api import consumers
from api.views import main_api_view, ApplicationModelViewSet, HelpOfferingModelViewSet
from api.views import UserApiViewModelSet, UserImageViewSet
from api.views.application import ApplicationImageViewSet, TagsListView
from api.views.chat import ChatViewSet, MessagesViewSet
from api.views.help_offering import HelpOfferingImagesModelViewSet, AcceptHelpOffering

# from api.views.user import LikeUserView

schema_view = get_schema_view(
    openapi.Info(
        title="Referal API",
        default_version="v1",
        description="HELP ME API",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="zanzydnd@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

user_router = routers.SimpleRouter()

user_router.register("user", UserApiViewModelSet, basename="User")

application_for_help_serializer = routers.DefaultRouter()
application_for_help_serializer.register(r'help_application', ApplicationModelViewSet, basename='HelpApplication')

help_offering_nested_router = routers.NestedDefaultRouter(application_for_help_serializer, "help_application",
                                                          lookup='help_application')

help_offering_nested_router.register("offer_help", HelpOfferingModelViewSet, basename="help-offering")
urlpatterns = [
    path("", main_api_view),
    path("auth/", auth_views.obtain_auth_token),
    *user_router.urls,
    *application_for_help_serializer.urls,
    *help_offering_nested_router.urls,
    path("help_application/<int:help_application_id>/image/",
         ApplicationImageViewSet.as_view(actions={"put": "update"})),
    path("tags/", TagsListView.as_view(actions={"get": "list"})),
    path("help_application/<int:help_application_id>/image/<int:pk>/",
         ApplicationImageViewSet.as_view(actions={"delete": "destroy"})),
    path("help_application/<int:help_application_pk>/offer_help/<int:offered_help_pk>/accept/",
         AcceptHelpOffering.as_view(actions={"put": "update"})),
    path("user/<int:user_id>/image/", UserImageViewSet.as_view(actions={"put": "update"})),
    path("user/<int:user_id>/image/<int:pk>/", UserImageViewSet.as_view(actions={"delete": "destroy"})),
    # path("user/<int:user_id>/like/", LikeUserView.as_view(actions={"post": "create"})),
    path("help_application/<int:help_application_id>/offered_help/<int:offered_help_id>/image/",
         HelpOfferingImagesModelViewSet.as_view(actions={"put": "update"})),
    path("help_application/<int:help_application_id>/offered_help/<int:offered_help_id>/image/<int:pk>/",
         HelpOfferingImagesModelViewSet.as_view(actions={"delete": "destroy"})),
    path("chat/", ChatViewSet.as_view(actions={"get": "list"})),
    path("chat/<int:pk>/", ChatViewSet.as_view(actions={"get": "retrieve"})),
    path("chat/<int:chat_id>/messages/", MessagesViewSet.as_view(actions={"get": "list"})),
    path("chat/<int:chat_id>/messages/<int:pk>/read/", MessagesViewSet.as_view(actions={"put": "update"})),
    re_path("swagger(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]

websocket_urlpatterns = [
    re_path(r'ws/chat/(?P<room_code>\w+)/$', consumers.ChatConsumer.as_asgi()),
]

urlpatterns += websocket_urlpatterns
