import string
from random import randint

alphabet = list(string.ascii_lowercase) + list(string.ascii_uppercase)


def create_random_chars():
    return "".join(alphabet[randint(0, 33)] for i in range(randint(20, 100)))
