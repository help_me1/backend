import datetime


def change_filename(instance, filename):
    name_original, ext = filename.split(".")

    return f"{name_original}_{datetime.datetime.now().strftime('%m_%d_%Y_%H_%M_%S')}.{ext}"