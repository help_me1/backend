from django.contrib import admin

# Register your models here.
from api.models import TagsForApplication,OfferedHelp,ApplicationForHelp,User


admin.site.register(TagsForApplication)
admin.site.register(ApplicationForHelp)
admin.site.register(User)
admin.site.register(OfferedHelp)
